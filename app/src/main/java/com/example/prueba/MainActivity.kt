package com.example.prueba

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.speech.tts.TextToSpeech
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    lateinit var buttonIniciar: Button
    lateinit var buttonPuntos: Button
    lateinit var txtScore: TextView
    lateinit var txtNumero: TextView
    var tick: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        buttonIniciar = findViewById(R.id.btnIniciar)
        buttonPuntos = findViewById(R.id.btnPuntos)
        txtScore = findViewById(R.id.txtPuntaje)
        txtNumero = findViewById(R.id.txtNumero)

        buttonIniciar.setOnClickListener {
            generarNumero()
        }

        buttonPuntos.setOnClickListener {
            aumentarScore()
        }
    }

    val timer = object: CountDownTimer(10000, 1000) {
        override fun onTick(millisUntilFinished: Long) {
            Log.d("seconds remaining: ", "" + millisUntilFinished / 1000)
            tick = (millisUntilFinished / 1000).toInt()
        }

        override fun onFinish() {
            Toast.makeText(applicationContext, "Numero actualizado", Toast.LENGTH_SHORT).show()
            generarNumero()
        }
    }

    private fun generarNumero() {
        val n = (0..10).shuffled().first()
        txtNumero.text = n.toString()
        timer.start()
    }

    private fun aumentarScore() {
        var numeroAcertar:Int = txtNumero.text.toString().toInt()
        var score = txtScore.text.toString().toInt()

        Log.e("TICK", tick.toString())
        Log.e("NUMERO", numeroAcertar.toString())
        if(tick == numeroAcertar) {
            score = score + 100
            txtScore.text = score.toString()
        } else if((tick-numeroAcertar) == 1 || ((tick-numeroAcertar)) == -1) {
            score = score + 50
            txtScore.text = score.toString()
        }
    }

}
